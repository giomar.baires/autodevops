- [ ] Problem: what is the problem
- [ ] How to replicate: how can the dev recreate the issue 
- [ ] Proposed solution: is there a solution already? Or does the dev need to look into it
- [ ] Who logged the problem: which QA/or team member the dev can get in touch with for more info or clarification on the task 
- [ ] Images/videos: screenshots of error messages/issues or video to help 



**Steps to Reproduce:**


**Actual Result:**


**Expected Result:**


| Device Details |     |
| :------------- | --: |
| Device Type    |     |
| App Version    |     |
| Date Found     |     |


# To Do
- [ ] FE
- [ ] BE
- [ ] QA
- [ ] QA

